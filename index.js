var request = require('request');
var json2csv = require('json2csv');
var fs = require('fs');

var fields = [
  'id',
  'timestamp',
  'path',
  'pathIndex',
  'isComplete',
  'username',
  'ucdPersonUuid',
  'firstName',
  'lastName',
  'emailAddress',
  'isFaculty',
  'isStaff',
  'isStudent',
  'building',
  'buildingKey',
  'room',
  'comment',
  'isFavorite',
  'utm_campaign',
  'utm_source',
  'comfort',
  'satisfaction',
  'conserveVote1',
  'conserveVote1b',
  'conserveVote2',
  'conserveVote2b',
  'conserveVote3',
  'conserveVote3b'
];

var todaysDate = new Date();
todaysDate.setHours(0,0,0,0);  //set to midnight

var endDateParsed = todaysDate.toJSON().slice(0,10);

todaysDate.setDate(todaysDate.getDate() - 1);   //day before
var startDateParsed = todaysDate.toJSON().slice(0,10);

var urlPath = 'http://thermoostat.ucdavis.edu/thermoostat/campusSummary?apiKey=lolwut&startTime='+startDateParsed+'%1200:00:00&endTime='+endDateParsed+'%1200:00:00';

console.log('Requesting:'+urlPath);

request(urlPath, function (error, response, body) {
  if (!error && response.statusCode == 200) {
    var jsonData = JSON.parse(body);
    try {
      var result = json2csv({ data: jsonData, fields: fields, unwindPath: 'path'});
      fs.writeFile(process.env.HOME+'/Box Sync/TherMOOstat CSVs/'+startDateParsed+".csv", result, function(err) {
        if(err) {
            return console.log(err);
        }else {
          console.log("The CSV file was saved!");
        }
      });
    } catch (err) {
      // Errors are thrown for bad options, or if the data is empty and no fields are provided.
      console.error(err);
    }
  }
});
